'use strict';

angular
  .module('testAngularjsPdfApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'pdf'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/angularjspdf', {
        templateUrl: 'views/angularjspdf.html',
        controller: 'AngularjspdfCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
