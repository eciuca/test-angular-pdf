'use strict';

angular.module('testAngularjsPdfApp')
  .controller('AngularjspdfCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.selectedPdf = 1;
    $scope.pdfUrl = 'images/' + $scope.selectedPdf + '.pdf';

        $scope.$watch('selectedPdf', function (newVal) {
            $scope.pdfUrl = 'images/' + newVal + '.pdf';
        });


  });
